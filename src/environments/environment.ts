// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAEI_FIlvroshg7dawsyz3nITuRPsGA_mg',
    authDomain: 'smartstud-1c419.firebaseapp.com',
    databaseURL: 'https://smartstud-1c419.firebaseio.com',
    projectId: 'smartstud-1c419',
    storageBucket: 'smartstud-1c419.appspot.com',
    messagingSenderId: '695791517852',
    appId: '1:695791517852:web:ddc5caa27479c8e7112d59',
    measurementId: 'G-64VQCGRDNH'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
