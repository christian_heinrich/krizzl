import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { User } from "../auth/user";
import { ApiService } from "../shared/services/api.service";
import { GameAlgorithmService } from "../shared/services/game-algorithm.service";

const firebase = require("firebase/app");
require("firebase/auth");

@Component({
  selector: "app-login-screen",
  templateUrl: "./login-screen.component.html",
  styleUrls: ["./login-screen.component.css"]
})
export class LoginScreenComponent implements OnInit {
  user: User;

  constructor(
    private api: ApiService,
    private gameAlgorithm: GameAlgorithmService,
    private toastr: ToastrService
  ) {}
  mobile: boolean;
  myPlayer: any;

  ngOnInit() {
    // Anonymous Login with Firebase, to get Access to Firestore
    firebase
      .auth()
      .signInAnonymously()
      .catch(function(error) {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorCode, errorMessage);
      });

    if (window.screen.width < 480) {
      this.mobile = true;
    }
  }

  downloadAPK() {
    // hier den Downloadlink bereitstellen
  }

  async login(player: string) {
    await this.gameAlgorithm.checkForHost(player).then(myPlayer => {
      this.myPlayer = myPlayer;
      this.toastr.success("you can now play as " + myPlayer.name, "Nice!");
      localStorage.setItem("myPlayer", JSON.stringify(myPlayer));
    });
  }

  onResize(event) {
    if (event.target.innerWidth < 480) {
      this.mobile = true;
    } else {
      this.mobile = false;
    }
  }
}
