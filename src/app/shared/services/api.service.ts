import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const firebaseURL = 'https://us-central1-smartstud-1c419.cloudfunctions.net/api';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  // this functions dont get used anymore, because the game-logic is client side

  public getBackendFunction() {
    return this.http.get(firebaseURL + '/say/hello');
  }

  public postImOnline(user: string) {
    // tslint:disable-next-line: object-literal-shorthand
    return this.http.post(firebaseURL + '/imOnline', { user: user }, { responseType: 'text' });
  }

  public getOnlineUsers() {
    return this.http.get(firebaseURL + '/onlineUsers');
  }
}



