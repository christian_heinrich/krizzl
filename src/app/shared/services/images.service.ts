import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {

  constructor(private firestore: AngularFirestore,
    private db: AngularFireDatabase) {
  }

  getImages() {
    return this.firestore.collection('Base64Strings').snapshotChanges();
  }

  getRTDBImages() {
    return this.db.object('Base64Strings/imageID/image').valueChanges();
  }

}
