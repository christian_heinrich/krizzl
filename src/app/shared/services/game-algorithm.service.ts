import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

const firebase = require('firebase/app');
// Required for side-effects
require('firebase/firestore');

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
  apiKey: 'AIzaSyAEI_FIlvroshg7dawsyz3nITuRPsGA_mg',
  authDomain: 'smartstud-1c419.firebaseapp.com',
  databaseURL: 'https://smartstud-1c419.firebaseio.com',
  projectId: 'smartstud-1c419',
  storageBucket: 'smartstud-1c419.appspot.com',
  messagingSenderId: '695791517852',
  appId: '1:695791517852:web:ddc5caa27479c8e7112d59',
  measurementId: 'G-64VQCGRDNH'
});

const db = firebase.firestore();

let id: string;
let myPlayerObj: any;

@Injectable({
  providedIn: 'root'
})
export class GameAlgorithmService {
  constructor(private firestore: AngularFirestore) { }

  getPlayers() {
    return this.firestore.collection('players').valueChanges();
  }

  getMessages() {
    return this.firestore.collection('chat').valueChanges();
  }

  getMyPlayer(myPlayer) {
    return this.firestore
      .collection('players')
      .doc(myPlayer.id)
      .valueChanges();
  }

  getTime() {
    return this.firestore
      .collection('time')
      .doc('timeID')
      .valueChanges();
  }

  getRound() {
    return this.firestore
      .collection('time')
      .doc('round')
      .valueChanges();
  }

  async sendChatandGuess(myPlayer, message) {
    let returnObject: { guessedOrNot: boolean; returnWord: string };
    await db
      .collection('words')
      .doc('wordToGuess')
      .get()
      .then(doc => {
        const rightGuesses = doc.data().rightGuesses;
        const word = doc.data().word;
        if (message === 'TIME IS OVER!') {
          returnObject = { guessedOrNot: false, returnWord: word };
          return returnObject;
        }
        if (message === word) {
          returnObject = { guessedOrNot: true, returnWord: word };
          db.collection('players')
            .doc(myPlayer.id)
            .get()
            .then(player => {
              const points = player.data().points;
              if (player.data().canGuess) {
                if (rightGuesses === 0) {
                  db.collection('players')
                    .doc(myPlayer.id)
                    .update({
                      points: points + 100,
                      canGuess: false
                    });
                } else if (rightGuesses === 1) {
                  db.collection('players')
                    .doc(myPlayer.id)
                    .update({
                      points: points + 70,
                      canGuess: false
                    });
                } else if (rightGuesses === 2) {
                  db.collection('players')
                    .doc(myPlayer.id)
                    .update({
                      points: points + 50,
                      canGuess: false
                    });
                } else {
                  db.collection('players')
                    .doc(myPlayer.id)
                    .update({
                      points: points + 25,
                      canGuess: false
                    });
                }
              } else {
                console.log('You can not guess right now!');
              }
              db.collection('words')
                .doc('wordToGuess')
                .update({ rightGuesses: rightGuesses + 1 });
            });
        } else {
          // if the word is not the right word, send the message to the public chat
          returnObject = { guessedOrNot: false, returnWord: 'Guess again' };
          db.collection('chat')
            .get()
            .then(snap => {
              const size = snap.size; // will return the collection size
              const docName = (size + 1).toString();
              db.collection('chat')
                .doc(docName)
                .set({
                  name: myPlayer.name,
                  value: message,
                  sortNumber: docName
                });
            });
        }
      })
      .catch(err => {
        console.log('Error getting document', err);
      });
    return returnObject;
  }

  resetRigthGuesses() {
    db.collection('words')
      .doc('wordToGuess')
      .update({ rightGuesses: 0 });
  }

  checkForHost(player: string) {
    let isHost: boolean;
    let canDraw: boolean;
    let canGuess: boolean;

    const myPlayer = db
      .collection('players')
      .get()
      .then(snap => {
        const size = snap.size; // will return the collection size to determine if you can be the host or not
        if (size === 0) {
          isHost = true;
          canDraw = true;
          canGuess = false;
          id = '1';
        } else {
          isHost = false;
          canDraw = false;
          canGuess = true;
          id = (size + 1).toString();
        }
        this.createPlayer(player, isHost, canDraw, canGuess, id);
        myPlayerObj = {
          id,
          name: player,
          points: 0,
          canDraw,
          isHost,
          canGuess
        };
        return myPlayerObj;
      });
    return myPlayer;
  }
  createPlayer(
    player: string,
    isHost: boolean,
    canDraw: boolean,
    canGuess: boolean,
    id: string
  ) {
    db.collection('players')
      .doc(id)
      .set({
        id,
        name: player,
        points: 0,
        canDraw,
        isHost,
        canGuess
      });
  }

  async setRandomWord(): Promise<string> {
    const wordRef = db.collection('words').doc('wordStorage');
    let rndWord: string;
    await wordRef
      .get()
      .then(doc => {
        if (!doc.exists) {
          console.log('No such document!');
        } else {
          const max = Object.keys(doc.data()).length;
          const wordNumber = Math.floor(Math.random() * Math.floor(max));
          const randomWord = doc.data()[wordNumber];
          db.collection('words')
            .doc('wordToGuess')
            .update({ word: randomWord });
          rndWord = randomWord;
        }
      })
      .catch(err => {
        console.log('Error getting document', err);
      });
    return rndWord;
  }

  playRound(player1Id, player2Id) {
    return new Promise((resolve, reject) => {
      let i = 45;
      const refreshIntervalId = setInterval(() => {
        db.collection('time')
          .doc('timeID')
          .update({
            time: i
          });
        i--;
        if (i === -1) {
          clearInterval(refreshIntervalId);
          db.collection('time')
            .doc('timeID')
            .update({
              time: 'TIME IS OVER!'
            });
          db.collection('players')
            .doc(player1Id)
            .update({
              canDraw: false,
              canGuess: true
            });
          db.collection('players')
            .doc(player2Id)
            .update({
              canDraw: true,
              canGuess: false
            });
          resolve(player1Id);
        }
      }, 1000);
    });
  }

  resetCanGuess(players) {
    players.forEach(player => {
      db.collection('players')
        .doc(player.id)
        .update({ canGuess: true });
    });
  }

  increaseRound(round) {
    db.collection('time')
      .doc('round')
      .update({
        round: round + 1
      });
  }

  deleteEverything() {
    db.collection('players')
      .get()
      .then(snap => {
        const size = snap.size; // will return the collection size
        for (let i = 1; i <= size; i++) {
          db.collection('players')
            .doc(i.toString())
            .delete()
            .then(() => {
              console.log('Document successfully deleted!');
            })
            .catch(error => {
              console.error('Error removing document: ', error);
            });
        }
      });

    db.collection('chat')
      .get()
      .then(snap => {
        const size = snap.size; // will return the collection size
        for (let i = 1; i <= size; i++) {
          db.collection('chat')
            .doc(i.toString())
            .delete()
            .then(() => {
              console.log('Document successfully deleted!');
            })
            .catch(error => {
              console.error('Error removing document: ', error);
            });
        }
      });

    db.collection('time')
      .doc('round')
      .update({
        round: 0
      });

    localStorage.removeItem('myPlayer');
  }
}
