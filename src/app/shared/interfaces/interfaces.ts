export interface Players {
    id: number;
    name: string;
    points: number;
    canDraw: boolean;
    isHost: boolean;
    canGuess: boolean;
}
