import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IngameComponent } from '../app/ingame/ingame.component';
import { LoginScreenComponent } from '../app/login-screen/login-screen.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginScreenComponent,
  },
  {
    path: 'ingame',
    component: IngameComponent,
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }, // redirect to the login-screen
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
