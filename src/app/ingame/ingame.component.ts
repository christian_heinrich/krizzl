import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { Images } from '../images.model';
import { ApiService } from '../shared/services/api.service';
import { GameAlgorithmService } from '../shared/services/game-algorithm.service';
import { ImagesService } from '../shared/services/images.service';

@Component({
  selector: 'app-ingame',
  templateUrl: './ingame.component.html',
  styleUrls: ['./ingame.component.css']
})
export class IngameComponent implements OnInit {
  images: Images[];
  // decodedImages: any[];
  image: any;
  players: any[];
  messages: any[];
  data: any[];
  myPlayer: any;
  time: any;
  round: any;
  randomWord = '';

  constructor(
    private imagesService: ImagesService,
    private gameAlgorithm: GameAlgorithmService,
    private sanitizer: DomSanitizer,
    private api: ApiService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.players = [];
    this.messages = [];

    // get myPlayer form local Browser Storage
    if (localStorage.getItem('myPlayer') !== null) {
      this.myPlayer = JSON.parse(localStorage.getItem('myPlayer'));
    }

    this.gameAlgorithm.getPlayers().subscribe(players => {
      this.players = players;
    });

    this.gameAlgorithm.getTime().subscribe(time => {
      this.time = time;
      if (
        this.time.time === 'TIME IS OVER!' &&
        this.round !== 0 &&
        !this.myPlayer.canDraw
      ) {
        this.gameAlgorithm
          .sendChatandGuess(this.myPlayer, 'TIME IS OVER!')
          .then(word => {
            this.toastr.info(
              'The time is over! ' +
              'The painter was looking for: ' +
              word.returnWord
            );
          });
      }
    });

    this.gameAlgorithm.getRound().subscribe(round => {
      this.round = round;
    });

    this.getPlayerUpdates();

    this.gameAlgorithm.getMessages().subscribe(messages => {
      this.messages = messages;
      this.testScrollDown();
      function compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const messageA = Number(a.sortNumber);
        const messageB = Number(b.sortNumber);

        let comparison = 0;
        if (messageA > messageB) {
          comparison = 1;
        } else if (messageA < messageB) {
          comparison = -1;
        }
        return comparison;
      }
      this.messages.sort(compare);
    });

    this.imagesService.getRTDBImages().subscribe(images => {
      const objectURL = 'data:image/png;base64,' + images;
      this.image = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    });
  }

  getPlayerUpdates() {
    if (this.myPlayer !== undefined) {
      this.gameAlgorithm
        .getMyPlayer(this.myPlayer)
        .subscribe(async myPlayer => {
          this.myPlayer = myPlayer;
          if (this.myPlayer.canDraw && this.randomWord === '') {
            this.randomWord = await this.gameAlgorithm.setRandomWord();
          } else if (
            !this.myPlayer.canDraw &&
            this.time.time === 'TIME IS OVER!'
          ) {
            this.randomWord = '';
          }
        });
    }
  }

  testScrollDown() {
    setTimeout(() => {
      const container = document.getElementById('ChatDiv');
      container.scrollTop = container.scrollHeight;
    }, 200);

  }

  async login(player: string) {
    await this.gameAlgorithm.checkForHost(player).then(myPlayer => {
      this.myPlayer = myPlayer;
      localStorage.setItem('myPlayer', JSON.stringify(myPlayer));
    });
    this.getPlayerUpdates();
  }

  async chatAndGuess(message) {
    this.gameAlgorithm
      .sendChatandGuess(this.myPlayer, message)
      .then(guessedOrNot => {
        (document.getElementById('chatEingabeID') as HTMLInputElement).value =
          '';
        if (guessedOrNot.guessedOrNot) {
          this.toastr.success('Your guess was right! ' + 'Nice!');
        }
      });
  }

  async startDrawing() {
    this.gameAlgorithm.increaseRound(this.round.round);
    const i = this.players.length;
    if (Number(this.myPlayer.id) === i) {
      await this.playRound(this.myPlayer.id, '1');
    } else {
      await this.playRound(
        this.myPlayer.id,
        (Number(this.myPlayer.id) + 1).toString()
      );
    }
  }

  finishGame() {
    this.gameAlgorithm.deleteEverything();
  }

  async playRound(i, i2) {
    this.gameAlgorithm.resetCanGuess(this.players);
    this.gameAlgorithm.resetRigthGuesses();
    await this.gameAlgorithm.playRound(i, i2);
  }
}
